const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('./config/config');
const datasource = require('./config/datasource');
const Email = require ('./utils/email');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const authRouter = require('./routes/auth');
const racesRouter = require('./routes/races');
const sexesRouter = require('./routes/sexes');
const namesRouter = require('./routes/names');
const lastnamesRouter = require('./routes/lastnames');
const randomizeRouter = require('./routes/randomize');

const authorization = require('./auth');

const app = express();
const port = 3333;
app.set('port', port);

app.config = config;
app.datasource = datasource(app);

app.email = new Email(app.config);

app.use(bodyParser.json({
  limit: '5mb'
}));

app.use(cors());

const auth = authorization(app);
app.use(auth.initialize());
app.auth = auth;

indexRouter(app);
usersRouter(app);
authRouter(app);
racesRouter(app);
sexesRouter(app);
namesRouter(app);
lastnamesRouter(app);
randomizeRouter(app);

module.exports = app;