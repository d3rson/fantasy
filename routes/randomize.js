

const RandomizeController = require('../controllers/randomize');

module.exports = (app) => {
  const randomizeController = new RandomizeController(app.datasource.models.names, app.datasource.models.lastnames);
  
  app.route('/generate/:raceId/:sexId')
  .all(app.auth.authenticate())
  .get((req, res) => {
    randomizeController
      .get(req.params)
      .then(rs => {
        res.json(rs);
      })
      .catch(error => {
        console.error(error.message);
        res.status(error.status);
      })
  });
}