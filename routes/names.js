const NamesController = require('../controllers/names');

module.exports = (app) => {

  const namesController = new NamesController(app.datasource.models.names, app.datasource.models.races, app.datasource.models.sexes);

  app.route('/names')
    .all(app.auth.authenticate())
    .get((req, res) => {
			namesController
				.getAll()
				.then(rs => {
					res.json(rs.data)
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
    })
    .post((req, res) => {
			namesController
				.create(req.body)
				.then(rs => {
					res.json(rs.data)
					res.status(rs.status)
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
    });

    app.route('/names/:id')
		.all(app.auth.authenticate())
		.get((req, res) => {
			namesController
				.getById(req.params)
				.then(rs => {
					res.json(rs.data);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		})
		.put((req, res) => {
			namesController
				.update(req.body, req.params)
				.then(rs => {
					res.json(rs.data);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		})
		.delete((req, res) => {
			namesController
				.delete(req.params)
				.then(rs => {
					res.json(rs.data);
					res.status(rs.status);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		});

}