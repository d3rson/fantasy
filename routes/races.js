const RacesController = require('../controllers/races');

module.exports = (app) => {

  const racesController = new RacesController(app.datasource.models.races);

  app.route('/races')
    .all(app.auth.authenticate())
    .get((req, res) => {
			racesController
				.getAll()
				.then(rs => {
					res.json(rs.data)
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
    })
    .post((req, res) => {
			racesController
				.create(req.body)
				.then(rs => {
					res.json(rs.data)
					res.status(rs.status)
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
    });

    app.route('/races/:id')
		.all(app.auth.authenticate())
		.get((req, res) => {
			racesController
				.getById(req.params)
				.then(rs => {
					res.json(rs.data);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		})
		.put((req, res) => {
			racesController
				.update(req.body, req.params)
				.then(rs => {
					res.json(rs.data);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		})
		.delete((req, res) => {
			racesController
				.delete(req.params)
				.then(rs => {
					res.json(rs.data);
					res.status(rs.status);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		});

}