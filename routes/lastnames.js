const LastnamesController = require('../controllers/lastnames');

module.exports = (app) => {

  const lastnamesController = new LastnamesController(app.datasource.models.lastnames, app.datasource.models.races);

  app.route('/lastnames')
    .all(app.auth.authenticate())
    .get((req, res) => {
			lastnamesController
				.getAll()
				.then(rs => {
					res.json(rs.data)
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
    })
    .post((req, res) => {
			lastnamesController
				.create(req.body)
				.then(rs => {
					res.json(rs.data)
					res.status(rs.status)
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
    });

    app.route('/lastnames/:id')
		.all(app.auth.authenticate())
		.get((req, res) => {
			lastnamesController
				.getById(req.params)
				.then(rs => {
					res.json(rs.data);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		})
		.put((req, res) => {
			lastnamesController
				.update(req.body, req.params)
				.then(rs => {
					res.json(rs.data);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		})
		.delete((req, res) => {
			lastnamesController
				.delete(req.params)
				.then(rs => {
					res.json(rs.data);
					res.status(rs.status);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		});

}