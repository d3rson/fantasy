const SexesController = require('../controllers/sexes');

module.exports = (app) => {

  const sexesController = new SexesController(app.datasource.models.sexes);

  app.route('/sex')
    .all(app.auth.authenticate())
    .get((req, res) => {
			sexesController
				.getAll()
				.then(rs => {
					res.json(rs.data)
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
    })
    .post((req, res) => {
			sexesController
				.create(req.body)
				.then(rs => {
					res.json(rs.data)
					res.status(rs.status)
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
    });

    app.route('/sex/:id')
		.all(app.auth.authenticate())
		.get((req, res) => {
			sexesController
				.getById(req.params)
				.then(rs => {
					res.json(rs.data);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		})
		.put((req, res) => {
			sexesController
				.update(req.body, req.params)
				.then(rs => {
					res.json(rs.data);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		})
		.delete((req, res) => {
			sexesController
				.delete(req.params)
				.then(rs => {
					res.json(rs.data);
					res.status(rs.status);
				})
				.catch(error => {
					console.error(error.message);
					res.status(error.status);
				})
		});

}