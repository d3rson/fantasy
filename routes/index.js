module.exports = (app) => {
  app.route('/')
  .all(app.auth.authenticate())
  .get((req, res) =>{
    res.json({
      message: "Hello World!"
    })
  })
}