const HttpStatus = require('http-status');

const defaultResponse = (data, status = HttpStatus.OK) => ({
	data,
	status
})

const errorResponse = (message, status = HttpStatus.BAD_REQUEST) => defaultResponse({
	error: message,
	status
}, status);

class LastnamesController{
  constructor(modelName, modelRace) {
    this.Lastnames = modelName;
    this.Races = modelRace;
  }
  
  getAll() {
		return this.Lastnames
			.findAll({
        include: [
          {
            model: this.Races
          }
        ]
      })
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message));
  }
  
  getById(params) {
		return this.Lastnames
			.findOne({ where: params })
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message));
  }
  
  create(data) {
		return this.Lastnames
			.create(data)
			.then(rs => defaultResponse(rs, HttpStatus.CREATED))
			.catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
  
  update(data, params) {
		return this.Lastnames
			.update({
        name: data.name,
        raceId: data.raceId,
			}, { where: params })
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
  
  delete(params) {
		return this.Lastnames
			.destroy({ where: params })
			.then(rs => defaultResponse(rs, HttpStatus.NO_CONTENT))
			.catch(e => errorResponse(e.message));
	}

}

module.exports = LastnamesController;