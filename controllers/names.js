const HttpStatus = require('http-status');

const defaultResponse = (data, status = HttpStatus.OK) => ({
	data,
	status
})

const errorResponse = (message, status = HttpStatus.BAD_REQUEST) => defaultResponse({
	error: message,
	status
}, status);

class NamesController{
  constructor(modelName, modelRace, modelSex) {
    this.Names = modelName;
    this.Races = modelRace;
    this.Sexes = modelSex;
  }
  
  getAll() {
		return this.Names
			.findAll({
        include: [
          {
            model: this.Sexes
          },
          {
            model: this.Races
          }
        ]
      })
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message));
  }
  
  getById(params) {
		return this.Names
			.findOne({ where: params })
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message));
  }
  
  create(data) {
		return this.Names
			.create(data)
			.then(rs => defaultResponse(rs, HttpStatus.CREATED))
			.catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
  
  update(data, params) {
		return this.Names
			.update({
        name: data.name,
        raceId: data.raceId,
        sexId: data.sexId
			}, { where: params })
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
  
  delete(params) {
		return this.Names
			.destroy({ where: params })
			.then(rs => defaultResponse(rs, HttpStatus.NO_CONTENT))
			.catch(e => errorResponse(e.message));
	}

}

module.exports = NamesController;