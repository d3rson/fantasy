const HttpStatus = require('http-status');

const defaultResponse = (data, status = HttpStatus.OK) => ({
	data,
	status
})

const errorResponse = (message, status = HttpStatus.BAD_REQUEST) => defaultResponse({
	error: message,
	status
}, status);

class RacesController{
  constructor(modelRace) {
		this.Races = modelRace;
  }
  
  getAll() {
		return this.Races
			.findAll({})
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message));
  }
  
  getById(params) {
		return this.Races
			.findOne({ where: params })
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message));
  }
  
  create(data) {
		return this.Races
			.create(data)
			.then(rs => defaultResponse(rs, HttpStatus.CREATED))
			.catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
  
  update(data, params) {
		return this.Races
			.update({
				name: data.name
			}, { where: params })
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
  
  delete(params) {
		return this.Races
			.destroy({ where: params })
			.then(rs => defaultResponse(rs, HttpStatus.NO_CONTENT))
			.catch(e => errorResponse(e.message));
	}

}

module.exports = RacesController;