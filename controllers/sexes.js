const HttpStatus = require('http-status');

const defaultResponse = (data, status = HttpStatus.OK) => ({
	data,
	status
})

const errorResponse = (message, status = HttpStatus.BAD_REQUEST) => defaultResponse({
	error: message,
	status
}, status);

class SexesController{
  constructor(modelSex) {
		this.Sexes = modelSex;
  }
  
  getAll() {
		return this.Sexes
			.findAll({})
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message));
  }
  
  getById(params) {
		return this.Sexes
			.findOne({ where: params })
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message));
  }
  
  create(data) {
		return this.Sexes
			.create(data)
			.then(rs => defaultResponse(rs, HttpStatus.CREATED))
			.catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
  
  update(data, params) {
		return this.Sexes
			.update({
				name: data.name
			}, { where: params })
			.then(rs => defaultResponse(rs))
			.catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
  
  delete(params) {
		return this.Sexes
			.destroy({ where: params })
			.then(rs => defaultResponse(rs, HttpStatus.NO_CONTENT))
			.catch(e => errorResponse(e.message));
	}

}

module.exports = SexesController;