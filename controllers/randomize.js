const Sequelize = require('sequelize');
const dbConfig = require('../config/config');

const sequelize = new Sequelize(
  dbConfig.database,
  dbConfig.username,
  dbConfig.password,
  dbConfig.params
);

const HttpStatus = require('http-status');

const defaultResponse = (data, status = HttpStatus.OK) => ({
	data,
	status
})

const errorResponse = (message, status = HttpStatus.BAD_REQUEST) => defaultResponse({
	error: message,
	status
}, status);

class RandomController{
  constructor(modelName, modelLastname) {
    this.Names = modelName;
    this.Lastnames = modelLastname
  }
  
  async get ({raceId, sexId}){

    const randName = await this.Names
      .findOne({order: sequelize.random(), limit: 1, where: {raceId, sexId}})
      .then(randomName =>{
        return randomName;
      });
    
    const randLastname = await this.Lastnames
      .findOne({order: sequelize.random(), limit: 1, where: {raceId}})
      .then(randomLastname =>{
        return randomLastname;
      });
 
    const random = randName.name + ' ' + randLastname.name

    return(random) 
    
  }
}

module.exports = RandomController;