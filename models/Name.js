module.exports = (sequelize, DataType) => {

  const Name = sequelize.define('names', {
    id: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    raceId:{
      type: DataType.INTEGER,
      allowNull: false
    },
    sexId:{
      type: DataType.INTEGER,
      allowNull: false
    }
  });

  return Name;

}