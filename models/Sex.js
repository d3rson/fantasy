const argon2 = require('argon2');

module.exports = (sequelize, DataType) => {

  const Sex = sequelize.define('sexes', {
    id: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    }
  });

  return Sex;

}