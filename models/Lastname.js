module.exports = (sequelize, DataType) => {

  const Lastname = sequelize.define('lastnames', {
    id: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    raceId:{
      type: DataType.INTEGER,
      allowNull: false
    }
  });

  return Lastname;

}